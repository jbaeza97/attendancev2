from django.test import TestCase
from .models import Student, Teacher, Subject, ClassGroup, Registry, RegistryDetail
import datetime as dt

# Create your tests here.
class StudentModelTest(TestCase):

  def test_create_student_in_database(self):
    """
    test_create_student_in_database() returns True if the student was successfuly added to the database.
    """
    student = Student()
    student.name = 'Joel Baeza Aranda'
    student.email = 'ldda18b.jbaeza@uartesdigitales.edu.mx'
    student.save()

    student_in_db = Student.objects.get(pk=1)

    return self.assertEqual(student, student_in_db)

  def test_update_student_in_database(self):
    """
    test_update_student_in_database() returns True if the student was successfuly updated in the database.
    """
    student = Student()
    student.name = 'Juan Pablo Coronado'
    student.email = 'ldda17c.jcoronado@uartesdigitales.edu.mx'
    student.save()
    
    update_student = Student.objects.get(pk=1)
    update_student.name = 'Juan Pablo Coronado Rosales'
    update_student.save()

    return self.assertEqual(update_student, Student.objects.get(pk=1))

  def test_delete_student_in_database(self):
    """
    test_delete_student_in_database() returns True if the student was successfuly deleted in the database.
    """
    student = Student()
    student.name = 'Kelvin Mandujano Toscano'
    student.email = 'ldda17c.kmandujano@uartesdigitales.edu.mx'
    student.save()

    deleted_student = Student.objects.get(pk=1)
    deleted_student.delete()

    return self.assertEqual(Student.objects.all().count(), 0)

class TeacherModelTest(TestCase):

  def test_create_teacher_in_database(self):
    """
    test_create_teacher_in_database() returns True if the teacher was successfuly added to the database.
    """
    teacher = Teacher()
    teacher.name = 'Emmanuel Alejandro Parada Licea'
    teacher.email = 'eparada@uartesdigitales.edu.mx'
    teacher.save()

    teacher_in_db = Teacher.objects.get(pk=1)

    return self.assertEqual(teacher, teacher_in_db)

  def test_update_teacher_in_database(self):
    """
    test_update_teacher_in_database() returns True if the teacher was successfuly updated in the database.
    """
    teacher = Teacher()
    teacher.name = 'Luis Manuel Morales'
    teacher.email = 'lmorales@uartesdigitales.edu.mx'
    teacher.save()
    
    update_teacher = Teacher.objects.get(pk=1)
    update_teacher.name = 'Luis Manuel Morales Medina'
    update_teacher.save()

    return self.assertEqual(update_teacher, Teacher.objects.get(pk=1))

  def test_delete_teacher_in_database(self):
    """
    test_delete_teacher_in_database() returns True if the teacher was successfuly deleted in the database.
    """
    teacher = Teacher()
    teacher.name = 'Oswaldo Luna'
    teacher.email = 'oluna@uartesdigitales.edu.mx'
    teacher.save()

    deleted_teacher = Teacher.objects.get(pk=1)
    deleted_teacher.delete()

    return self.assertEqual(Teacher.objects.all().count(), 0)

class SubjectModelTest(TestCase):

  def test_create_subject_in_database(self):
    """
    test_create_subject_in_database() returns True if the subject was successfuly added to the database.
    """
    subject = Subject()
    subject.name = 'Desarrollo de Aplicaciones II'
    subject.weekHours = 4
    subject.save()

    subject_in_db = Subject.objects.get(pk=1)

    return self.assertEqual(subject, subject_in_db)

  def test_update_subject_in_database(self):
    """
    test_update_subject_in_database() returns True if the subject was successfuly updated in the database.
    """
    subject = Subject()
    subject.name = 'Prototipos'
    subject.weekHours = 4
    subject.save()
    
    update_subject = Subject.objects.get(pk=1)
    update_subject.name = 'Internet de las cosas'
    update_subject.save()

    return self.assertEqual(update_subject, Subject.objects.get(pk=1))

  def test_delete_subject_in_database(self):
    """
    test_delete_subject_in_database() returns True if the subject was successfuly deleted in the database.
    """
    subject = Subject()
    subject.name = 'Ingeniería de Software'
    subject.weekHours = 4
    subject.save()

    deleted_subject = Subject.objects.get(pk=1)
    deleted_subject.delete()

    return self.assertEqual(Subject.objects.all().count(), 0)

class ClassGroupModelTest(TestCase):

  def test_create_class_group_in_database(self):
    """
    test_create_class_group_in_database() returns True if the class_group was successfuly added to the database.
    """
    student = Student()
    student.name = 'Joel Baeza Aranda'
    student.email = 'ldda18b.jbaeza@uartesdigitales.edu.mx'
    student.save()

    group = ClassGroup()
    group.name = "LDDA 6to"
    group.save()

    group_in_db = ClassGroup.objects.get(pk=1)

    return self.assertEqual(group, group_in_db)

  def test_update_class_group_in_database(self):
    """
    test_update_class_group_in_database() returns True if the class_group was successfuly updated in the database.
    """
    group = ClassGroup()
    group.name = 'LDDA 6to'
    group.save()
    
    update_group = ClassGroup.objects.get(pk=1)
    update_group.name = 'LDDA 6to GE'
    update_group.save()

    return self.assertEqual(update_group, ClassGroup.objects.get(pk=1))

  def test_delete_class_group_in_database(self):
    """
    test_delete_class_group_in_database() returns True if the class_group was successfuly deleted in the database.
    """
    group = ClassGroup()
    group.name = 'LDDA 6to'
    group.save()

    deleted_group = ClassGroup.objects.get(pk=1)
    deleted_group.delete()

    return self.assertEqual(ClassGroup.objects.all().count(), 0)

class RegistryModelTest(TestCase):

  def test_create_registry_in_database(self):
    """
    test_create_registry_in_database() returns True if the registry was successfuly added to the database.
    """
    subject = Subject()
    subject.name = 'Prototipos'
    subject.weekHours = 4
    subject.save()

    teacher = Teacher()
    teacher.name = 'Luis Manuel Morales Medina'
    teacher.email = 'lmorales@uartesdigitales.edu.mx'
    teacher.save()

    group = ClassGroup()
    group.name = "LDDA 6to"
    group.save()

    registry = Registry()
    registry.session_start = dt.time(7, 00)
    registry.session_end = dt.time(8, 40)
    registry.session_date = dt.date(2019, 5, 23)
    registry.subject = Subject.objects.get(pk=1)
    registry.class_group = ClassGroup.objects.get(pk=1)
    registry.teacher = Teacher.objects.get(pk=1)
    registry.save()
    
    return self.assertEqual(registry, Registry.objects.get(pk=1))

class RegistryDetailModelTest(TestCase):

  def test_create_registry_detail_in_database(self):
    """
    test_create_registry_detail_in_database() returns True if the registry_detail was successfuly added to the database.
    """
    subject = Subject()
    subject.name = 'Prototipos'
    subject.weekHours = 4
    subject.save()

    teacher = Teacher()
    teacher.name = 'Luis Manuel Morales Medina'
    teacher.email = 'lmorales@uartesdigitales.edu.mx'
    teacher.save()

    group = ClassGroup()
    group.name = "LDDA 6to"
    group.save()

    registry = Registry()
    registry.session_start = dt.time(7, 00)
    registry.session_end = dt.time(8, 40)
    registry.session_date = dt.date(2019, 5, 23)
    registry.subject = Subject.objects.get(pk=1)
    registry.class_group = ClassGroup.objects.get(pk=1)
    registry.teacher = Teacher.objects.get(pk=1)
    registry.save()

    student = Student()
    student.name = 'Joel Baeza Aranda'
    student.email = 'ldda18b.jbaeza@uartesdigitales.edu.mx'
    student.save()

    registry_detail = RegistryDetail()
    registry_detail.student = Student.objects.get(pk=1)
    registry_detail.is_checked = True
    registry_detail.registry = Registry.objects.get(pk=1)
    registry_detail.save()

    return self.assertEqual(registry_detail, RegistryDetail.objects.get(pk=1))