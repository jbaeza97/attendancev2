from django.urls import path, include
from django.contrib.auth import views as web_views
from . import views

urlpatterns = [
     # Web routes
    path('', web_views.LoginView.as_view(template_name='attendance/login.html')),
    path('login/', web_views.LoginView.as_view(template_name='attendance/login.html'), name='web-login'),
    path('logout/', web_views.LogoutView.as_view(template_name='attendance/logout.html'), name='web-logout'),
    path('profile/', views.home, name="attendance-home")
]
