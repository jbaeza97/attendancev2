# Generated by Django 2.2.1 on 2019-06-04 06:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attendance', '0005_remove_schoolcycle_registry'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='registrydetail',
            name='attendance',
        ),
        migrations.AddField(
            model_name='registrydetail',
            name='missings',
            field=models.IntegerField(default=0),
        ),
    ]
