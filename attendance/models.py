from django.db import models
import datetime as dt

HOUR_CHOICES = [
  (dt.time(7, 0), "7:00"), (dt.time(7, 50), "7:50"),
  (dt.time(8, 40), "8:40"), (dt.time(9, 30), "9:30"),
  (dt.time(10, 20), "10:20"), (dt.time(11, 10), "11:10"),
  (dt.time(12, 00), "12:00"), (dt.time(12, 50), "12:50"),
  (dt.time(13, 40), "13:40"), (dt.time(14, 30), "14:30"),
  (dt.time(15, 20), "15:20"), (dt.time(16, 10), "16:10"),
  (dt.time(17, 00), "17:00"), (dt.time(17, 50), "17:50"),
  (dt.time(18, 40), "18:40"), (dt.time(19, 30), "19:30"),
  (dt.time(20, 20), "20:20"), (dt.time(21, 10), "21:10"),
  (dt.time(22, 00), "22:00")
  ]
# Create your models here.
class Student(models.Model):
  first_name = models.CharField(max_length=50)
  last_name = models.CharField(max_length=50, default="None")
  email = models.EmailField()

  def __str__(self):
    return f"{self.first_name} {self.last_name}"

class Teacher(models.Model):
  first_name = models.CharField(max_length=50)
  last_name = models.CharField(max_length=50, default="None")
  email = models.EmailField()
  
  def __str__(self):
    return f"{self.first_name} {self.last_name}"

class Subject(models.Model):
  name = models.CharField(max_length=100)
  weekHours = models.IntegerField()

  def __str__(self):
    return f"{self.name}"


class ClassGroup(models.Model):
  name = models.CharField(max_length=100)
  student = models.ManyToManyField(Student)

  def __str__(self):
    return f"{self.name}"

class Registry(models.Model):
  session_start = models.TimeField((u"Inicio de clase"), choices=HOUR_CHOICES)
  session_end = models.TimeField((u"Termino de clase"), choices=HOUR_CHOICES)
  session_date = models.DateField((u"Día de clase"))
  session_hours_count = models.IntegerField(default=1)
  subject = models.ForeignKey(Subject, on_delete=None)
  class_group = models.ForeignKey(ClassGroup, on_delete=None)
  teacher = models.ForeignKey(Teacher, on_delete=None)

  def __str__(self):
    return f"{self.session_date} - {self.class_group} - {self.subject} - {self.teacher} - {self.session_start} - {self.session_end}"

class RegistryDetail(models.Model):
  student = models.ForeignKey(Student, on_delete=None)
  missings = models.IntegerField(default=0)
  registry = models.ForeignKey(Registry, on_delete=None)

  def __str__(self):
    return f"{self.student} | {self.registry}"

class SchoolCycle(models.Model):
  name = models.CharField(max_length=10)

  def __str__(self):
    return f"{self.name}"

class SchoolCyclePartial(models.Model):
  name = models.CharField(max_length=10)
  partial_begin = models.DateField(u"Inicio de Cuatrimestre")
  partial_end = models.DateField(u"Termino de Cuatrimestre")
  school_cycle = models.ForeignKey(SchoolCycle, on_delete=None)

  def __str__(self):
    return f"{self.name} begins {self.partial_begin} and ends {self.partial_end}. School cycle: {self.school_cycle}"
