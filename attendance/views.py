from django.shortcuts import render
from django.contrib.auth.models import User, Group
from django.contrib.auth import authenticate
from django.http import JsonResponse
from rest_framework import viewsets, generics
from rest_framework.views import status
from rest_framework.response import Response

# Models and Serializers imports
from .serializers import *
from .models import *

# TOTP imports for code
import random as rand
import string
import pyotp 

class LoginView(generics.CreateAPIView):
  """
  API endpoint that ensures who logs into the app and deny the access of unknown users not registered in the database.
  """
  def post(self, request):
    user = authenticate(username=request.data['username'], password=request.data['password'])
    if user is not None:
      try:
        if Student.objects.get(email=request.data['email']):
          student = Student.objects.get(email=request.data['email'])
          return JsonResponse(data = {"role": "student", "first_name": student.first_name, 'id': student.id }, status = status.HTTP_200_OK)
      except Student.DoesNotExist:
        try:
          if Teacher.objects.get(email=request.data['email']):
            teacher = Teacher.objects.get(email=request.data['email'])
            return JsonResponse(data = {"role": "teacher", "first_name": teacher.first_name, 'id': teacher.id}, status = status.HTTP_200_OK)
        except Teacher.DoesNotExist:
          return JsonResponse(data = {"message": "User is not registered for any classes"})
    else:
      return JsonResponse(
        data = {"message": "User logged doesn't exists"},
        status = status.HTTP_401_UNAUTHORIZED )

class ListCreateCodeView(generics.ListCreateAPIView):
  def get(self, *args, **kwargs):
    """
    API endpoint that returns the TOTP code to the teacher and student.
    Secret must be base32 encoded. 
    """
    totp = pyotp.TOTP('OVQWIMRQGE4WE===', interval=15)
    return JsonResponse(totp.now(), safe=False)

  def post(self, request, *args, **kwargs):
    """
    API endpoint that saves a student whose code is valid.
    """
    totp = pyotp.TOTP('OVQWIMRQGE4WE===', interval=15)
    validation = int(request.data['code'])
    if totp.verify(validation, valid_window=1):
      return JsonResponse(request.data['code'], safe=False)
    return JsonResponse({"msg": "Wrong code"})

class ListCreateStudentView(generics.ListCreateAPIView):
  """
  GET students/
  POST students/
  """
  queryset = Student.objects.all()
  serializer_class = StudentSerializer

  def post(self, request, *args, **kwargs):
    student = Student.objects.create(
      name=request.data["name"],
      email=request.data["email"]
    )
    return Response(
      data=StudentSerializer(student).data,
      status=status.HTTP_201_CREATED
    )

class ListCreateRegistryView(generics.ListCreateAPIView):
  """
  GET registries/
  POST registries/
  """
  queryset = Registry.objects.all()
  serializer_class = RegistrySerializer

  def post(self, request, *args, **kwargs):
    registry = Registry.objects.create(
      session_start=request.data["session_start"],
      session_end=request.data["session_end"],
      session_date=request.data["session_date"],
      session_hours_count=request.data["session_hours_count"],
      subject=request.data["subject"],
      class_group=request.data["class_group"],
      teacher=request.data["teacher"],
    )
    return Response(
      data=RegistrySerializer(registry).data,
      status=status.HTTP_201_CREATED
    )

class RegistryDetailView(generics.RetrieveUpdateDestroyAPIView):
  """
  GET registries/:id/
  PUT registries/:id/
  """
  queryset = Registry.objects.all()
  serializer_class = RegistrySerializer

  def get(self, request, *args, **kwargs):
    try:
      registry = self.queryset.filter(teacher_id=kwargs['pk']).order_by("-session_date")[:5]
      return JsonResponse(RegistrySerializer(registry, many=True).data, safe=False) 
    except Registry.DoesNotExist:
      return JsonResponse(
        data = {
          "message": "Registry with id: {} does not exist".format(kwargs["pk"])
        },
        status = status.HTTP_404_NOT_FOUND
      )
  
  def put(self, request, *args, **kwargs):
    try:
      registry = self.queryset.get(pk=kwargs["pk"])
      serializer = RegistrySerializer()
      updated_registry = serializer.update(registry, request.data)
      return Response(RegistrySerializer(updated_registry).data)
    except Registry.DoesNotExist:
      return Response(
        data={
          "message": "Registry with id: {} does not exist".format(kwargs["pk"])
        },
        status=status.HTTP_404_NOT_FOUND
      )

class RegistryStudentDetailView(generics.RetrieveUpdateDestroyAPIView):
  """
  GET registry-student/:id/
  PUT registry-student/:id/
  """
  queryset = ClassGroup.objects.all()
  serializer_class = RegistrySerializer

  def get(self, request, *args, **kwargs):
    try:
      groups = self.queryset.filter(student=kwargs['pk'])
      filtered_groups = ClassGroupSerializer(groups, many=True).data
      # registries = Registry.objects.filter(class_group=filtered_groups.id)
      return JsonResponse(filtered_groups, safe=False)

      # return JsonResponse(ClassGroupSerializer(group, many=True).data, safe=False) 
    except Registry.DoesNotExist:
      return JsonResponse(
        data = {
          "message": "Registry with id: {} does not exist".format(kwargs["pk"])
        },
        status = status.HTTP_404_NOT_FOUND
      )
  
  def put(self, request, *args, **kwargs):
    try:
      registry = self.queryset.get(pk=kwargs["pk"])
      serializer = RegistrySerializer()
      updated_registry = serializer.update(registry, request.data)
      return Response(RegistrySerializer(updated_registry).data)
    except Registry.DoesNotExist:
      return Response(
        data={
          "message": "Registry with id: {} does not exist".format(kwargs["pk"])
        },
        status=status.HTTP_404_NOT_FOUND
      )

class RegistryDetailViewSet(viewsets.ModelViewSet):
  """
  API endpoint that allows users to be viewed or edited.
  """
  queryset = RegistryDetail.objects.all()
  serializer_class = RegistryDetailSerializer

class RegistrySpecificDetailView(generics.RetrieveUpdateAPIView):
  """
  GET registry-detail/:id/
  PUT registry-detail/:id/
  DELETE registry-detail/:id/
  """
  queryset = RegistryDetail.objects.all()
  serializer_class = RegistryDetailSerializer

  def get(self, request, *args, **kwargs):
    try:
      registry_detail = self.queryset.filter(registry_id=kwargs['pk'])
      return JsonResponse(RegistryDetailSerializer(registry_detail, many=True).data, safe=False) 
    except RegistryDetail.DoesNotExist:
      return JsonResponse(
        data = {
          "message": "Registry detail with id: {} does not exist".format(kwargs["pk"])
        },
        status = status.HTTP_404_NOT_FOUND
      )
  
  def put(self, request, *args, **kwargs):
    try:
      registry_detail = self.queryset.get(pk=kwargs["pk"])
      serializer = RegistryDetailSerializer()
      updated_registry_detail = serializer.update(registry_detail, request.data)
      return Response(RegistryDetailSerializer(updated_registry_detail).data)
    except RegistryDetail.DoesNotExist:
      return Response(
        data={
          "message": "Registry detail with id: {} does not exist".format(kwargs["pk"])
        },
        status=status.HTTP_404_NOT_FOUND
      )

class UserViewSet(viewsets.ModelViewSet):
  """
  API endpoint that allows users to be viewed or edited.
  """
  queryset = User.objects.all().order_by('-date_joined')
  serializer_class = UserSerializer

class GroupViewSet(viewsets.ModelViewSet):
  """
  API endpoint that allows groups to be viewed or edited.
  """
  queryset = Group.objects.all()
  serializer_class = GroupSerializer

# Web views
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

@login_required
def home(request, *args, **kwargs):
  teacher = Teacher.objects.get(email=request.user.email)
  registries = Registry.objects.filter(teacher_id=teacher.id).order_by('-session_date')
  registry_details = RegistryDetail.objects.filter(registry_id=teacher.id)
  return render(request, 'attendance/home.html', {'registries': registries, 'details': registry_details})
  