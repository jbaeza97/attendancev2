from django.contrib import admin
from .models import Student, Teacher, Subject, ClassGroup, Registry, RegistryDetail, SchoolCycle, SchoolCyclePartial

# Register your models here.
# Contraseñas para las cuentas generadas para testing - 9kOr5icr

class SearchDetailAdmin(admin.ModelAdmin):
  search_fields = ['student__first_name']

class SearchAdmin(admin.ModelAdmin):
  search_fields = ['teacher__first_name', 'class_group__name', 'subject__name']

admin.site.register(RegistryDetail, SearchDetailAdmin)
admin.site.register(Registry, SearchAdmin)
admin.site.register(Student)
admin.site.register(Teacher)
admin.site.register(Subject)
admin.site.register(ClassGroup)
admin.site.register(SchoolCycle)
admin.site.register(SchoolCyclePartial)