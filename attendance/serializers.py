from django.contrib.auth.models import User, Group
from rest_framework import serializers
from attendance.models import Student, Teacher, Subject, ClassGroup, Registry, RegistryDetail

class UserSerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model = User
    fields = ('url', 'username', 'email', 'groups', 'password')

class GroupSerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model = Group
    fields = ('url', 'name')
  
class StudentSerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model = Student
    fields = ('id', 'first_name', 'last_name', 'email')

class TeacherSerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model = Teacher
    fields = ('id', 'first_name', 'last_name', 'email')

class SubjectSerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model = Subject
    fields = ('name', 'weekHours')

class ClassGroupSerializer(serializers.HyperlinkedModelSerializer):
  student = StudentSerializer(many=True)
  class Meta:
    model = ClassGroup
    fields = ('id', 'name', 'student')

class RegistrySerializer(serializers.HyperlinkedModelSerializer):
  subject = SubjectSerializer()
  teacher = TeacherSerializer()
  class_group = ClassGroupSerializer()
  class Meta:
    model = Registry
    fields = ('id','session_start', 'session_end', 'session_date', 'session_hours_count', 'subject', 'class_group', 'teacher')

class RegistryDetailSerializer(serializers.ModelSerializer):
  student = StudentSerializer()
  registry = RegistrySerializer()
  class Meta:
    model = RegistryDetail
    fields = ('id', 'student', 'missings', 'registry')