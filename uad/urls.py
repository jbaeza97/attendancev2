"""uad URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.views.decorators.csrf import csrf_exempt
from rest_framework import routers
from attendance import views
from attendance.views import (
ListCreateRegistryView, RegistryDetailView,
ListCreateCodeView, LoginView,
RegistrySpecificDetailView, RegistryStudentDetailView
)

# No olvides hacer la presentación en la clase de dragonov y ademas añade información para otras clases dentro de la base de datos
# Tales como a Parada, Oswaldo y Teofilio


admin.site.site_header = "UAD Attendance Administration"
admin.site.site_title = "General Administration"
admin.site.index_title = "General Administration"


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'registry-detail', views.RegistryDetailViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    # App routes
    path('code/', ListCreateCodeView.as_view()),

    path('login/', csrf_exempt(LoginView.as_view())),
    
    path('registries/', ListCreateRegistryView.as_view()),
    path('registries/<int:pk>/', RegistryDetailView.as_view()),

    path('registry-student/<int:pk>/', RegistryStudentDetailView.as_view()),

    path('registry_details/<int:pk>', RegistrySpecificDetailView.as_view()),
    
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),

    path('attendance-web/', include('attendance.urls')),
]